import java.util.Stack;

public class Tower {
    private Stack<Integer> tower = new Stack<>();
    private int numer;

    public Tower(int numer) {
        this.numer = numer;
    }

    public void wypelnijWieze(int wielkosc) {
        for (int i = wielkosc; i > 0; i--) {
            tower.push(i);
        }
    }

    public void push(Krazek X) {
        if (tower.empty()) tower.push(X.getWielkosc());

        else if (X.getWielkosc() < tower.peek()) {
            tower.push(X.getWielkosc());
        } else {
            throw new TooBigKrazekException();
        }
    }

    public Krazek pop() {
        Integer y = tower.pop();
        Krazek x = new Krazek(y);
        return x;
    }

    public void show() {
        System.out.println("tower: " + numer + " " + tower);

    }

    public boolean isEmpty() {
        return tower.empty();
    }


}
