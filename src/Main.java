import java.util.Scanner;
import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        Hanoi gra = new Hanoi(3);
        Scanner sc = new Scanner(System.in);

        while (true) {
            gra.tower1.show();
            gra.tower2.show();
            gra.tower3.show();

            System.out.println("From:");
            String from = sc.nextLine();
            int fromint = Integer.parseInt(from)-1;
            System.out.println();

            System.out.println("To:");
            String to = sc.nextLine();
            int toint = Integer.parseInt(to)-1;
            System.out.println();

            Krazek przenoszony = gra.towers.get(fromint).pop();
            try {
                gra.towers.get(toint).push(przenoszony);
            } catch (TooBigKrazekException tbke) {
                gra.towers.get(fromint).push(przenoszony);
            }


            if (gra.tower2.isEmpty() && gra.tower1.isEmpty()) {
                System.out.println("gra skonczona, gratulacje !");
                break;
            }

        }
    }
}
